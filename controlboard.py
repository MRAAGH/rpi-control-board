#!/usr/bin/python3

import RPi.GPIO as gpio
import time
import os

# try block allows gpio cleanup when cancelled
try:
    gpio.setmode(gpio.BCM)

    # pins of connected buttons
    pins = [18, 23, 24, 25, 8, 7, 12, 16, 20]

    # some buttons require longer press than others.
    # abort, shutdown and self-destruct require 5 seconds.
    required_durations = [50, 50, 50, 10, 5, 5, 5, 5, 10]

    # the commands that get run when buttons are pressed.
    # this is using some scripts which can be found at:
    # https://gitlab.com/MRAAGH/d/-/blob/master/bin/octo
    # https://gitlab.com/MRAAGH/d/-/blob/master/bin/octorest
    button_commands = [
            'echo BOOM',
            'systemctl poweroff',
            'octo abort',
            'octo print',
            'octo connect',
            'octo preheatnozzle',
            'octo preheatbed',
            'octo feed',
            'octo cooldown'
    ]

    for pin in pins:
        gpio.setup(pin, gpio.IN, pull_up_down=gpio.PUD_UP)

    # start in invalid state
    state = 'invalid'
    pressed_button = 0

    print('Press ctrl-c to quit')

    while True:
        buttons = [not gpio.input(pin) for pin in pins]
        num_pressed = sum(buttons)
        if num_pressed == 0:
            # none of the buttons are pressed
            state = 'pending'
        elif num_pressed > 1:
            # more than 1 button is pressed! Panic!
            state = 'invalid'
        else:
            # exactly 1 button is pressed.
            which_button = buttons.index(True) # this one.
            if state == 'pending':
                # it was pending for a button. Now there is a button.
                state = 'button'
                pressed_button = which_button
                button_countdown = required_durations[which_button]
            elif state == 'button':
                # a button is already pressed from earlier
                if pressed_button != which_button:
                    # button changed! Panic!
                    state = 'invalid'
                else:
                    # all is okay. Continue countdown
                    button_countdown -= 1
                    if button_countdown < 1:
                        # hey, it's been pressed long enough!
                        os.system(button_commands[which_button])
                        # state is now invalid, wait for release
                        state = 'invalid'

        time.sleep(0.1)

finally:
    print('cleaning the pins before quitting.')
    gpio.cleanup()
