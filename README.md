# RPI control board

My own raspberry pi control board using GPIO.

![3D printer control board](http://mazie.rocks/img/selfdestruct_mounted-big.jpg)

Why did I make another one of these instead of using existing software?
If you make things on your own, you have a better understanding of how they work, and it really wasn't that difficult, it only took an hour or so to code.

It is very fail-proof, it requires buttons to be held for a certain amount of time, and if multiple buttons are pressed at once, it immediately goes into "invalid" state and waits until all buttons are released.
So you can't accidentally press multiple buttons.

To send commands to the local OctoPrint server, I am using these:
- https://gitlab.com/MRAAGH/d/-/blob/main/bin/octo
- https://gitlab.com/MRAAGH/d/-/blob/main/bin/octorest